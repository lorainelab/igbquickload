import optparse,sys
from Quickload import *

def checkForHelp(usage):
    "Print usage message if user supplies option -h or --help"
    usage = '%prog [FILE]\n\n  Write to FILE or stdout.\n\n'+usage
    parser=optparse.OptionParser(usage)
    parser.parse_args()

def makeQuickloadFileObjectsFromTableData(fname=None,sep="\t"):
    'Return a list of QuickloadFile objects using contents of fname. Fname is a sep-delimited plain text file where the first row of the table contains file tag attributes for an annots.xml file. Subsequent rows contain attribute values for creating QuickloadFile objects, one per row. The string "NA" indicates the corresponding attribute in the header should not be set. Use the string TRUE or FALSE to indicate true or false attribute values.'
    if not fname:
        fh = sys.stdin
    else:
        fh = open(fname,'r')
    i=1
    lst=[]
    data=fh.read()
    # Excel 2011 on Mac uses \r as line endings
    # Windows programs use \r\n as line endings
    # Unix programs use \n as line endings
    # https://gist.github.com/jennybc/0be7717c2b5b30088811
    lines=data.split('\n')
    if len(lines)==1: # true if made by Excel on a Mac.
        lines=data.split('\r')
    for line in lines:
        toks=line.rstrip().split(sep)
        if i == 1:
            header=toks
        else:
            quickload_file=parseValues(toks,header)
            lst.append(quickload_file)
        i=i+1
    if fname:
        fh.close()
    return lst

def parseValues(toks,header):
    "Create a new QuickloadFile object. Configure using values in toks as indicated by values in header. Toks and header are lists of strings. Toks and header have the same length. Ignores invalid options."
    if not len(toks)==len(header):
        raise AttributeError("Require two lists of strings that are the same length.")
    d={}
    i=0
    for attribute in header:
        value=toks[i]
        i=i+1
        if not value.upper()=='NA' or value=='': 
            if value[0]=='"' and value[-1]=='"':
                value=value[1:len(value)-1]
            d[attribute]=value
    if d.has_key("label_field") or d.has_key("show2tracks") or d.has_key("direction_type") or d.has_key("positive_strand_color") or d.has_key("negative_strand_color"):
        quickload_file=AnnotationFile() # subclass of QuickloadFile
    else:
        quickload_file=QuickloadFile()
    for attribute in d.keys():
        value=d[attribute]
        if attribute=="label_field":
            quickload_file.setLabelField(value)
        if attribute=="max_depth":
            quickload_file.setStackHeight(value)
        if attribute=="show2tracks":
            if value.lower()=="true":
                quickload_file.setShow2Tracks(True)
            elif value.lower()=="false":
                quickload_file.setShow2Tracks(False)
            else:
                raise AttributeError("Don't recognize show2tracks option: %s"%value)
        if attribute=='direction_type':
            quickload_file.setDirectionIndicator(value)
        if attribute=='positive_strand_color':
            quickload_file.setPositiveStrandColor(value)
        if attribute=='negative_strand_color':
            quickload_file.setNegativeStrandColor(value)
        if attribute=='name':
            quickload_file.setPath(value)
        if attribute=='title':
            quickload_file.setTrackName(value)
        if attribute=='description':
            quickload_file.setToolTip(value)
        if attribute=='load_hint':
            quickload_file.setLoadAll(True)
        if attribute=='background':
            quickload_file.setBackgroundColor(value)
        if attribute=='foreground':
            quickload_file.setForegroundColor(value)
        if attribute=='name_size':
            quickload_file.setTrackLabelFontSize(value)
        if attribute=='url':
            quickload_file.setTrackInfoUrl(value)
    return quickload_file

            
