
from Quickload import *


def makeQuickloadFilesForRNASeq(lsts=None,
                            folder=None,
                            deploy_dir=None,
                            include_FJ=True,
                            include_TH=False,
                            include_scaled_bedgraph=False,
                            include_unscaled_bedgraph=False,
                            include_scaled_bw_by_strand=False,
                            include_scaled_bw=False):
    """
    Function: Makes QuickloadFile objects for data sets in an RNA-Seq experiment
              involving multiple samples
    Args    : lsts - a list of lists containing configurations for each sample
              folder - name of the folder shown to IGB users where data sets are stored
                       To get IGB to show nested folders, insert a / character between
                       nested folders names, e.g., RNA-Seq/Flower SRP062420 creates
                       (in IGB) a folder naemd "Flower SRP062420" in a folder named
                       "RNA-Seq"
              deploy_dir - physical location of the folder where data sets are stored
                           on the server; relative to the genome directory
              include_FJ - whether or not to include Loraine Lab find_junctions output,
                           default is TRUE
    Returns : List of QuickloadFile objects

    Assumes that for each sample, there is:
         bam file - with alignments
         bedgraph.gz - with genome coverage graph, tabix-indexed
         bed.gz file - with junctions predicted by tophat, tabix-indexed
         FJ.bed.gz file - with junctions predicted by Loraine Lab find junctions program,
                          tabix-indexed (unless include_FJ option is FALSE)

    lsts - list of lists; each sub-list contains, in this order:
           1 - file name prefix for data files
                  e.g., c1 for c1.bam, c1.bedgraph.gz, c1.FJ.bed.gz, etc.
           2 - title for the data set; this becomes the track label in IGB
           3 - track color (foreground) in hexadecimal code, e.g., 000000 for black
           4 - background color in hexadecimal code, e.g., FFFFFF or ffffff for white
           5 - option URL of the data set; when users click this, a Web page opens to this
               URL. It can be relative to the root of the Quickload site or absolute
                   e.g., http://www.example.com (absolute) or
                         A_thaliana_Jun_2009/rnaseq (relative to Quickload site root)
    """

    quickload_files = []
    for lst in lsts:
        f = lst[0]
        sample=lst[1]
        foreground=lst[2]
        background=lst[3]
        url = None
        if len(lst)>4:
            url = lst[4]
        if len(lst)>5:
            tooltip=lst[5]+", "
        else:
            tooltip=""
        # alignments
        descr = tooltip + "read alignments"
        fn = deploy_dir + "/" + f + '.bam'
        title = folder + "/Reads/" + sample + " alignments"
        quickload_file = BamFile(path=fn,track_name=title,track_info_url=url,
                                 foreground_color=foreground,background_color=background,
                                 tool_tip=descr)
        quickload_files.append(quickload_file)

        # scaled coverage becgraph format
        if include_scaled_bedgraph:
            descr = tooltip + "scaled coverage graph"
            fn = deploy_dir + "/" + f + '.scaled.bedgraph.gz'
            title = folder + "/Graph - Scaled/" + sample + " scaled coverage"
            quickload_file = QuickloadFile(path=fn,track_name=title,track_info_url=url,
                                           foreground_color=foreground,background_color=background,
                                           tool_tip=descr)
            quickload_files.append(quickload_file)
        if include_unscaled_bedgraph:# coverage
            descr = tooltip + "coverage graph"
            fn = deploy_dir + "/" + f + '.bedgraph.gz'
            title = folder + "/Graph - Unscaled/" + sample + " unscaled coverage"
            quickload_file = QuickloadFile(path=fn,track_name=title,track_info_url=url,
                                           foreground_color=foreground,background_color=background,
                                           tool_tip=descr)
            quickload_files.append(quickload_file)
        # bw by strand scaled coverage
        if include_scaled_bw_by_strand:
            descr = tooltip + "forward strand scaled coverage graph"
            fn = deploy_dir + "/" + f + ".fwd.bw"
            title = folder + "/Graph - Scaled/"+ sample + " forward strand scaled coverage"
            quickload_file = QuickloadFile(path=fn,track_name=title,track_info_url=url,
                                           foreground_color=foreground,background_color=background,
                                           tool_tip=descr)
            quickload_files.append(quickload_file)
            descr = "reverse strand scaled coverage graph"
            fn = deploy_dir + "/" + f + ".rev.bw"
            title = folder + "/Graph - Scaled/"+ sample + " reverse strand scaled coverage"
            quickload_file = QuickloadFile(path=fn,track_name=title,track_info_url=url,
                                           foreground_color=foreground,background_color=background,
                                           tool_tip=descr)
            quickload_files.append(quickload_file)
        # bw scaled coverage, not strand-specific
        if include_scaled_bw:
            descr = tooltip + "scaled coverage graph"
            fn = deploy_dir + "/" + f + ".rnaseq.bw"
            title = folder + "/Graph - Scaled/"+ sample + " scaled coverage"
            quickload_file = QuickloadFile(path=fn,track_name=title,track_info_url=url,
                                           foreground_color=foreground,background_color=background,
                                           tool_tip=descr)
            quickload_files.append(quickload_file)
        if include_FJ:
            descr = tooltip + "junctions predicted by Loraine Lab FindJunctions, single-mapping reads only"
            fn = deploy_dir + "/" + f + '.FJ.bed.gz'
            title = folder + "/Junctions/" + sample + " LL junctions"
            quickload_file = JunctionFile(path=fn,track_name=title,track_info_url=url,
                                       foreground_color=foreground,background_color=background,
                                       tool_tip=descr)
            quickload_files.append(quickload_file)
        # tophat junctions
        if include_TH:
            descr = tooltip + "junctions predicted by TopHat"
            fn = deploy_dir + "/" + f + '.bed.gz'
            title = folder + "/Junctions/" + sample + " tophat junctions"
            quickload_file = JunctionFile(path=fn,track_name=title,track_info_url=url,
                                            foreground_color=foreground,background_color=background,
                                            tool_tip=descr)
            quickload_files.append(quickload_file)
    return quickload_files
