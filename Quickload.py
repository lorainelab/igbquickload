import sys,re,logging

color_regex=re.compile(r'[0-9a-fA-F]{6}')

class QuickloadFile():

    def __init__(self,path=None,
                 track_name=None,
                 track_label_font_size=None,
                 tool_tip=None,
                 load_all=None,
                 label_field=None,
                 background_color=None,
                 foreground_color=None,
                 stack_height=None,
                 direction_indicator=None,
                 positive_strand_color=None,
                 negative_strand_color=None,
                 track_info_url=None):
        self.setPath(path)
        self.setTrackName(track_name)
        self.setTrackLabelFontSize(track_label_font_size)
        self.setToolTip(tool_tip)
        self.setLoadAll(load_all)
        self.setBackgroundColor(background_color)
        self.setForegroundColor(foreground_color)
        self.setTrackInfoUrl(track_info_url)

    def setPath(self,path):
        """
        Function: Set the URL or path to data file's physical location
        Args    : path - URL address of the file, somewhere on the internet (absolute)
                         or path to the file, relative to the genome subdirectory
        Returns :

        ex) quickload_file.setPath('http://www.example.com/dataset.bed.gz')
            quickload_file.setPath('dataset.bedgraph.gz')
            quickload_file.setPath('RNA-Seq/reads.bam')

        """
        self._path = path

    def getPath(self):
        """
        Function: Get the URL or path to data file
        Args    :
        Returns : path - URL (absolute) or path to file, relative to genome directory
        """
        return self._path

    def setTrackName(self,track_name):
        """
        Function: Set display name for a track, shown in track label, Data Management Table,
                  and Available Data section of the IGB Data Access tab
        Args    : a string; forward slash characters indicate folders
        Returns :

        If you include forward slash characters, IGB will display the the file nested in
        folders.

        ex)
           quickload_file.setTrackName("RNA-Seq experiments/Graphs and Reads/Sample Reads")

        displays data set "Sample Reads" in folder "Graphs and Reads" inside folder
        "RNA-Seq experiments"

        Note that spaces are allowed in folder and data set names. Unicode may or may not
        supported - we have not tested this.
        """
        self._track_name = track_name

    def getTrackName(self):
        """
        Function: Get display name for a track, shown in track label, Data Management Table,
                  and Available Data section of the IGB Data Access tab
        Args    :
        Returns : a string; forward slash characters indicate folders
        """
        return self._track_name

    def setTrackLabelFontSize(self,size):
        """
        Function: Set the font size of track labels in the main view.
        Args    : size - a positive int
        Returns :
        """
        if size and (not type(size)==int or size<1):
            raise ValueError("setTrackLabelFontSize requires positive int or None. Instead, given: "+ size)
        self._track_label_font_size = size

    def getTrackLabelFontSize(self):
        """
        Function: Get the font size of track labels in the main view.
        Args    :
        Returns : size - a positive int
        """
        return self._track_label_font_size

    def setToolTip(self,tool_tip):
        """
        Function: Set text of tooltip shown when user mouses over name of
                  a data set name in Available Data section of Data Access tab
        Args    : tool_tip - a string or None
        Returns :
        """
        self._tool_tip = tool_tip

    def getToolTip(self):
        """
        Function: Get text of tooltip shown when user mouses over name of
                  a data set name in Available Data section of Data Access tab
        Args    :
        Returns : tool_tip - a string or None
        """
        return self._tool_tip

    def setLoadAll(self,load_all):
        """
        Function: Set to load all the data when user opens the genome.
        Args    : load_all - True, False or None
        Returns :

        Same as setting the Load Mode to Genome in IGB Data Access tab
        Data Management table. This should be set to True for reference
        gene models.
        Entire data set must load to enable keyword and name searching.
        Also triggers local caching of remote files.
        """
        self._load_all = load_all

    def getLoadAll(self):
        """
        Function: Find out if data set is configured to load all the data when
                  user opens the genome.
        Args    :
        Returns : True, False or None

        Same as setting the Load Mode to Genome in IGB Data Access tab
        Data Management table. Typically True for reference gene models.
        Note that searching will only find data that are already loaded.
        Also triggers caching of remote files.
        """
        return self._load_all

    def setBackgroundColor(self,background_color):
        """
        Function: Set background color for track
        Args    : background_color - string, color in hexadecimal format
                    or None
        Returns :

        example) quickload_file.setBackgroundColor('ffffff') # white
        """
        if background_color and not color_regex.match(background_color):
            raise ValueError("setBackgroundColor requires hexadecimal color. Instead, given: "+
                             background_color)
        self._background_color = background_color

    def getBackgroundColor(self):
        """
        Function: Get background color for track
        Args    :
        Returns : background_color - string representing color in hexadecimal format
                      or None
        """
        return self._background_color

    def setForegroundColor(self,foreground_color):
        """
        Function: Set foreground color for track
        Args    : foreground_color - string representing color in hexadecimal format
                      or None
        Returns :

        example) quickload_file.setForegroundColor('000000') # black
        """
        if foreground_color and not color_regex.match(foreground_color):
            raise ValueError("setForegroundColor requires hexadecimal color. Instead, given: "+
                             foreground_color)
        self._foreground_color = foreground_color

    def getForegroundColor(self):
        """
        Function: Get foreground color for track
        Args    :
        Returns : foreground_color - string representing color in hexadecimal format
                      or None
        """
        return self._foreground_color

    def setTrackInfoUrl(self,track_info_url):
        self._track_info_url = track_info_url

    def getTrackInfoUrl(self):
        return self._track_info_url

    def getFileTagAttributes(self):
        lst=[]
        if self.getPath():
            lst.append(['name',self.getPath()])
        else:
            raise ValueError("Requires path to file.")
        if self.getTrackName():
            lst.append(['title',self.getTrackName()])
        else:
            raise ValueError("Requires track name.")
        if self.getToolTip():
            lst.append(['description',self.getToolTip()])
        if self.getTrackInfoUrl():
            lst.append(['url',self.getTrackInfoUrl()])
        if self.getLoadAll():
            lst.append(['load_hint','Whole Sequence'])
        if self.getBackgroundColor():
            lst.append(['background',self.getBackgroundColor()])
        if self.getForegroundColor():
            lst.append(['foreground',self.getForegroundColor()])
        if self.getTrackLabelFontSize():
            lst.append(['name_size',self.getTrackLabelFontSize()])
        return lst

class AnnotationFile(QuickloadFile):

    def __init__(self,stack_height=None,show2tracks=None,
                 direction_indicator=None,label_field=None,
                 positive_strand_color=None,negative_strand_color=None,
                 **kwargs):
        QuickloadFile.__init__(self,**kwargs)
        self.setStackHeight(stack_height)
        self.setShow2Tracks(show2tracks)
        self.setDirectionIndicator(direction_indicator)
        self.setLabelField(label_field)
        self.setPositiveStrandColor(positive_strand_color)
        self.setNegativeStrandColor(negative_strand_color)

    def setLabelField(self,label_field):
        self._label_field = label_field

    def getLabelField(self):
        return self._label_field

    def setStackHeight(self,stack_height):
        if type(stack_height) in (int,type(None)):
            if type(stack_height) == type(int):
                if stack_height < 0:
                    raise ValueError("setStackHeight: positive integer required")
                else:
                    self._stack_height = stack_height
            else:
                self._stack_height = stack_height
        else:
            raise ValueError("setStackHeight requires None or  positive integer")

    def getStackHeight(self):
        return self._stack_height

    def setShow2Tracks(self,show2tracks):
        if show2tracks not in (True,False,None):
            raise ValueError("setShow2Tracks requires True, False or None")
        self._show2tracks = show2tracks

    def getShow2Tracks(self):
        return self._show2tracks

    def setDirectionIndicator(self,direction_indicator):
        allowed_values=("arrow","color","both","none")
        if direction_indicator and not direction_indicator in allowed_values:
            raise ValueError("setDirectionIndicator requires None or one of %s"%' '.join(allowed_values))
        self._direction_indicator = direction_indicator

    def getDirectionIndicator(self):
        return self._direction_indicator

    def setPositiveStrandColor(self,positive_strand_color):
        if positive_strand_color and not color_regex.match(positive_strand_color):
            raise ValueError("setPositiveStrandColor requires None or string with hexadecimal color. Instead, given: "+positive_strand_color)
        self._positive_strand_color = positive_strand_color

    def getPositiveStrandColor(self):
        return self._positive_strand_color

    def setNegativeStrandColor(self,negative_strand_color):
        if negative_strand_color and not color_regex.match(negative_strand_color):
            raise ValueError("setNegativeStrandColor requires None or string with hexadecimal color")
        self._negative_strand_color = negative_strand_color

    def getNegativeStrandColor(self):
        return self._negative_strand_color

    def getFileTagAttributes(self):
        lst = QuickloadFile.getFileTagAttributes(self)
        if self.getLabelField():
            lst.append(['label_field',self.getLabelField()])
        if self.getStackHeight():
            lst.append(['max_depth',repr(self.getStackHeight())])
        if not self.getShow2Tracks()==None:
            if self.getShow2Tracks():
                lst.append(['show2tracks','true'])
            else:
                lst.append(['show2tracks','false'])
        if self.getDirectionIndicator():
            lst.append(['direction_type',self.getDirectionIndicator()])
        if self.getPositiveStrandColor():
            lst.append(['positive_strand_color',self.getPositiveStrandColor()])
        if self.getNegativeStrandColor():
            lst.append(['negative_strand_color',self.getNegativeStrandColor()])
        return lst

class BindingSite(AnnotationFile):
    "File containing binding sites from ChIP-seq experiment."
    def __init__(self,stack_height=2,direction_indicator="none",
                 show2tracks=False,
                 **kwargs):
        AnnotationFile.__init__(self,
                                stack_height=stack_height,
                                show2tracks=show2tracks,
                                direction_indicator=direction_indicator,
                                **kwargs)

class BamFile(AnnotationFile):
    "BAM file containing alignment features"

    def __init__(self,stack_height=10,show2tracks=False,
                 direction_indicator=None,**kwargs):
        AnnotationFile.__init__(self,stack_height=stack_height,
                                show2tracks=show2tracks,
                                direction_indicator=direction_indicator,
                                **kwargs)

class JunctionFile(AnnotationFile):
    "BED file containing junction features"

    def __init__(self,label_field="score",**kwargs):
        AnnotationFile.__init__(self,label_field=label_field,**kwargs)


## methods for making and writing XML

def makeFilesTag():
    return '<files>\n'

def closeFilesTag():
    return '</files>\n'

def makeFileTag(quickload_file):
    txt = '  <file\n'
    lsts = quickload_file.getFileTagAttributes()
    for lst in lsts:
        txt = txt + '    %s="%s"\n'%(lst[0],lst[1])
    txt = txt + "  />\n"
    return txt

def makeFileTagsXml(quickload_files):
    txt=''
    for quickload_file in quickload_files:
        txt = txt + makeFileTag(quickload_file)
    return txt

def makeAnnotsXml(quickload_files=None,
                  include_fnames=None):
    txt = makeFilesTag()
    txt = txt+makeFileTagsXml(quickload_files)
    if include_fnames:
            txt = txt + getIncludedText(include_fnames)
    txt = txt+closeFilesTag()
    return txt

def getIncludedText(include_fnames):
    txt=''
    for include in include_fnames:
        try:
            fh = open(include,'r')
            txt = txt + fh.read()
        except IOError:
            logging.warn("Can't include file %s. Skipping it."%include)
    return txt

def writeToFileStream(txt):
    fname=None
    if len(sys.argv)>1:
        fname=sys.argv[1] # Note: IGB expects it to be called annots.xml
    if fname:
        fh = open(fname,'w')
    else:
        fh = sys.stdout
    fh.write(txt)
    if fname:
        fh.close()


def main(quickload_files=None,
         include_fnames=None):
    txt = makeAnnotsXml(quickload_files=quickload_files,
                        include_fnames=include_fnames)
    writeToFileStream(txt)
