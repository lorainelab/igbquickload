#!/usr/bin/env python2.7

"""Make annots.xml for RNA-Seq Quickload site. Deploy files in directories. Make directories if they do not already exist"""


from Quickload import *
from AnnotsXmlForRNASeq import *
import QuickloadUtils as utils

# https://bitbucket.org/lorainelab/hotdryarabidopsis/src
import makeAnnotsXml as hotdry
import os,re


datasets=\
{"A_thaliana_Jun_2009":
 [["SRP062420","ant ail6 SRP062420",
   "336600","Ler 1,Ler_1.bam,Ler 2,Ler_2.bam,Ler 3,Ler_3.bam,Ler 4,Ler_4.bam",
   "9900FF","Mutant 1,AntAil6_1.bam,Mutant 2,AntAil6_2.bam,Mutant 3,AntAil6_3.bam,Mutant 4,AntAil6_4.bam"],
 ["SRP022162","Pollen SRP022162",
  "993300","Pollen,Pollen.bam",
  "008000","Leaf 1,Leaf1.bam,Leaf 2,Leaf2.bam"],
 ["SRP029896","Cold SRP029896",
  "4D4DFF","Cold treatment,ColdTreatment.bam",
  "006600","Control,ColdControl.bam"],
 ["SRP059384","Cytokinin SRP059384",
  "9900FF","Roots BA 1,SRR2061403.bam,Roots BA 2,SRR2061405.bam,Roots BA 3,SRR2061406.bam",
  "8B2323","Shoots BA 1,SRR2061400.bam,Shoots BA 2,SRR2061401.bam,Shoots BA 3,SRR2061402.bam",
  "1F78B4","Roots Mock 1,SRR2061397.bam,Roots Mock 2,SRR2061398.bam,Roots Mock 3,SRR2061399.bam",
  "33A02C","Shoots Mock 1,SRR2060631.bam,Shoots Mock 2,SRR2060632.bam,Shoots Mock 3,SRR2060634.bam"]],
  "S_lycopersicum_Feb_2017":
 [["SRP055068","Hot Pollen SRP055068",
  "336600","Control 1,SRR1805811.bam,Control 2,SRR1805718.bam,Control 3,SRR1805727.bam,Control 4,SRR1805729.bam,Control 5,SRR1805731.bam",
  "9900FF","Heat 1,SRR1805733.bam,Heat 2,SRR1805735.bam,Heat 3,SRR1805737.bam,Heat 4,SRR1805739.bam,Heat 5,SRR1805741.bam"]],
  "H_sapiens_Dec_2013":
  [["SRP056969","20 tissues SRP056969",
  "9900FF","Adrenal gland,SRR1957124_Adrenal_gland.bam",
  "336600","Brain Cerebellum,SRR1957125_Brain_Cerebellum.bam",
  "33A02C","Brain Whole,SRR1957183_Brain_Whole.bam",
  "8B2323","Brain Fetal,SRR1957186_Brain_Fetal.bam",
  "4D4DFF","Liver Fetal,SRR1957188_Liver_Fetal.bam",
  "006600","Heart,SRR1957191_Heart.bam",
  "FF9900","Kidney,SRR1957192_Kidney.bam",
  "593C1F","Liver,SRR1957193_Liver.bam",
  "336699","Lung,SRR1957195_Lung.bam",
  "663399","Placenta,SRR1957197_Placenta.bam",
  "FF9900","Prostate,SRR1957198_Prostate.bam",
  "F180C1","Salivary Gland,SRR1957200_Salivary_Gland.bam",
  "CC0000","Skeletal Muscle,SRR1957201_Skeletal_Muscle.bam",
  "134F5C","Small Intestine,SRR1957202_Small_Intestine.bam",
  "134F5C","Spleen,SRR1957203_Spleen.bam",
  "9900FF","Stomach,SRR1957205_Stomach.bam",
  "336600","Thymus,SRR1957206_Thymus.bam",
  "666666","Thyroid,SRR1957207_Thyroid.bam",
  "003366","Trachea,SRR1957208_Trachea.bam",
  "000000","Uterus,SRR1957209_Uterus.bam"]]
  }

def ensure_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def makeAnnotsXmlFiles(datasets=None):
    "Write annots.xml files for RNA-Seq genomic alignment files."
    for genome_version in datasets.keys():
        quickload_files=do_genome(genome_version=genome_version,
                                    datasets=datasets[genome_version])
        if genome_version == "A_thaliana_Jun_2009":
            hotdry_SRP220157_url = "https://bitbucket.org/lorainelab/hot-dry-arabidopsis/raw/master/ExternalDataSets/samples.txt"
            hotdry_quickload_files = hotdry.makeQuickloadFiles(fname=hotdry_SRP220157_url)
            for quickload_file in quickload_files:
                hotdry_quickload_files.append(quickload_file)
            quickload_files = hotdry_quickload_files
        txt = makeAnnotsXml(quickload_files=quickload_files)
        ensure_dir(genome_version)
        fname=genome_version+os.sep+"annots.xml"
        fh=open(fname,'w')
        fh.write(txt)
        fh.close()

def do_genome(genome_version=None,
              datasets=None):
    quickload_files=[]
    for dataset in datasets:
        physical_folder=dataset[0]
        url=genome_version+os.sep+physical_folder
        user_friendly_folder_name=dataset[1]
        lsts=[]
        i=2
        while i < len(dataset)-1:
            color=dataset[i]
            samples_string=dataset[i+1]
            sample_names=samples_string.split(',')
            j = 0
            while j < len(sample_names)-1:
                dataset_name=re.sub(".bam","",sample_names[j+1])
                lst=[dataset_name,sample_names[j],color,"ffffff",url]
                lsts.append(lst)
                j=j+2;
            i=i+2
        include_FJ = (not genome_version == 'H_sapiens_Dec_2013')
        include_scaled_bedgraph = (genome_version == 'H_sapiens_Dec_2013')
        newsamples=makeQuickloadFilesForRNASeq(lsts=lsts,folder=user_friendly_folder_name,
                                                deploy_dir=physical_folder,include_FJ=include_FJ,
                                                include_scaled_bedgraph=include_scaled_bedgraph)
        for item in newsamples:
            quickload_files.append(item)
    return quickload_files

if __name__ == '__main__':
    about="Make annots.xml text."
    utils.checkForHelp(about)
    makeAnnotsXmlFiles(datasets=datasets)
