# python version must be >= 3.6

from bs4 import BeautifulSoup
import requests, sys, math, os
from tqdm import tqdm
import xml.etree.ElementTree as et

def getQuickloadDataUrls(url):
    soup = BeautifulSoup(requests.get(url).text, "html.parser")
    return [url + i["href"] for i in soup.select("tr td:nth-child(2) a") if i.contents[0] != "Parent Directory" and "." in i["href"]]

def download_quickload_data(quickload_urls):
    """Download quickload data files matching the extensions specified in FILETYPES"""
    file_urls = []
    for qurl in quickload_urls:
        file_urls += getQuickloadDataUrls(qurl)
    print(f"Downloading {len(file_urls)} files.")
    for i in tqdm(range(len(file_urls))):
        url = file_urls[i]
        if any([t in url for t in FILETYPES]):
            filename = url.split("/")[-1]
            if os.path.isfile(OUTDIR + filename) or filename.endswith(".tbi"):
                continue
            with requests.get(url, stream=True) as r:
                if r.raise_for_status():
                    print(f"Failed to download {filename} due to \n{r.raise_for_status()}")
                    continue
                if not os.path.exists(OUTDIR):
                    os.mkdir(OUTDIR)
                with open(OUTDIR + filename, "wb") as f:
                    print(f"Downloading {filename}")
                    for chunk in tqdm(r.iter_content(chunk_size=CHUNK_SIZE), 
                        unit=" ~mb", unit_scale=CHUNK_SIZE/1000000, 
                        desc=f"{round(float(r.headers['content-length'])/1000000, 2)} MB"):
                        f.write(chunk)
                    print(f"Saved {filename} to {OUTDIR}")

def hexToRgb(hex):
    return ",".join([str(int(hex[i:i+2], 16)) for i in (0, 2, 4)])

def createTrackDb(quickload_urls):
    trackdb = ""
    track = 1
    for quickload_url in quickload_urls:
        tree = et.fromstring(requests.get(f"{quickload_url}/annots.xml").text)
        for el in tree.iter():
            if el.tag == "file":
                if "UCSCTracks" in el.attrib.get("name"):
                    continue
                ext = None
                if "bedgraph" in el.attrib.get('name'):
                    ext = "bw"
                    type = "bigWig"
                elif "bed" in el.attrib.get('name') or "psl" in el.attrib.get('name'):
                    ext = "bb"
                    type = "bigBed"
                elif "bam" in el.attrib.get('name'):
                    type = "bam"
                else:
                    print(f"STATUS: Skipping file with extension {'.'.join(el.attrib.get('name').split('.')[1:])}")
                    continue
                filename_base = ".".join(el.attrib.get("name").split("/")[-1].replace(".gz", "").split(".")[:-1])
                if ext:
                    data_url = f"https://data.cyverse.org/dav-anon/iplant/home/pbadzuh/human_hub/data/{filename_base}.{ext}"
                else:
                    data_url = f"{quickload_url}/{el.attrib.get('name')}"
                trackdb += f"track {track}_{filename_base.split('.')[0]}\n"
                trackdb += f"bigDataUrl {data_url}\n"
                trackdb += f"shortLabel {el.attrib.get('title')}\n"
                if el.attrib.get('description'):
                    trackdb += f"longLabel {el.attrib.get('description')}\n"
                else:
                    trackdb += f"longLabel {el.attrib.get('title')}\n"
                trackdb += f"type {type}\n"
                trackdb += f"html description\n"
                if el.attrib.get('foreground'):
                    trackdb += f"color {hexToRgb(el.attrib.get('foreground'))}\n"
                trackdb += "visibility dense\n"
                trackdb += "\n\n"
                track += 1
    return trackdb

HG38_URL="http://www.igbquickload.org/quickload/H_sapiens_Dec_2013/"
HG38_RNASEQ_URL="http://igbquickload.org/rnaseq/H_sapiens_Dec_2013/SRP056969/"
FILETYPES = [".psl.gz", ".bed.gz", ".bedgraph.gz"]
CHUNK_SIZE = 500000 # bytes
OUTDIR = "./out/" # trailing forward slash required

download_quickload_data([HG38_URL, HG38_RNASEQ_URL])

quickload_urls = ["http://www.igbquickload.org/quickload/H_sapiens_Dec_2013", "http://igbquickload.org/rnaseq/H_sapiens_Dec_2013"]
print(createTrackDb(quickload_urls))
