#gunzip out/*.gz
#mkdir -p converted/out
#The bedToBigBed, pslToBigPsl, and bedGraphToBigWig utilities are required and are expected to exist in th bin/ folder. They may be found at the following link: https://hgdownload.soe.ucsc.edu/admin/exe/

filename_without_extension () {
   echo $1 | rev | cut -d '.' -f 2- | rev
}

# Convert bed files to bigbed format
for FILE in $(find out/*.bed -exec basename {} \;)
do
  FILENAME=$(filename_without_extension $FILE)
  if [[ "$(head -n1 out/$FILE)" == *"JUNC"* ]]; then
    echo "Scaling junction file $FILE"
    cat out/$FILE | awk -v total="$(cat out/$FILE | awk '{print $5}' | sort -n | tail -1)" '{print $1"\t"$2"\t"$3"\t"$4"\t"int(0.5+(($5*1000)/total))"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12}' > converted/$FILENAME.scaled.bed
    ./bin/bedToBigBed converted/$FILENAME.scaled.bed http://igbquickload.org/rnaseq/H_sapiens_Dec_2013/genome.txt converted/$FILENAME.scaled.bb
    rm converted/*.bed
  else
    if [[ "$FILE" == *"hg38-blacklist.v2.bed"* ]] || [[ "$FILE" == *"H_sapiens_Dec_2013_snp153Common.bed"* ]]; then
      ./bin/bedToBigBed -type=bed4 -tab out/$FILE http://www.igbquickload.org/quickload/H_sapiens_Dec_2013/genome.txt converted/$FILENAME.bb
    elif [[ "$FILE" == *"H_sapiens_Dec_2013_ncbi"* ]] || [[ "$FILE" == *"H_sapiens_Dec_2013_refGene.bed"* ]]; then
      curl -LO "https://jira.bioviz.org/secure/attachment/16795/bedDetail.as"
      ./bin/bedToBigBed -as=bedDetail.as -type=bed12+2 -tab out/$FILE http://www.igbquickload.org/quickload/H_sapiens_Dec_2013/genome.txt converted/$FILENAME.bb
    else
      echo "ERROR: Unforseen file found"
      echo $FILENAME
    fi
  fi
done

# Convert non-standard bed files
for FILE in $(find out/hg38-blacklist.v2.bed -exec basename {} \;)
do
  ./bin/bedToBigBed -type=bed4 -tab out/$FILE http://igbquickload.org/rnaseq/H_sapiens_Dec_2013/genome.txt converted/$(filename_without_extension $FILE).bb
done

curl -LO "https://jira.bioviz.org/secure/attachment/16795/bedDetail.as"

for FILE in $({ find out/H_sapiens_Dec_2013_refGene.bed -exec basename {} \; & find out/H_sapiens_Dec_2013_ncbi* -exec basename {} \;; })
do
  ./bin/bedToBigBed -as=bedDetail.as -type=bed12+2 -tab out/$FILE http://igbquickload.org/rnaseq/H_sapiens_Dec_2013/genome.txt converted/$(filename_without_extension $FILE).bb
done

# Convert psl files to bigpsl format
for FILE in $(find out/H_sapiens*.psl -exec basename {} \;)
do
  ./bin/pslToBigPsl out/$FILE stdout | sort -k1,1 -k2,2n > converted/$(filename_without_extension $FILE).txt
done

curl -LO "https://genome.ucsc.edu/goldenPath/help/examples/bigPsl.as"
for FILE in $(find converted/H_sapiens*.txt -exec basename {} \;)
do
  ./bin/bedToBigBed -as=bigPsl.as -type=bed12+13 -tab converted/$FILE http://www.igbquickload.org/quickload/H_sapiens_Dec_2013/genome.txt converted/$(filename_without_extension $FILE).bb
done

rm converted/H_sapiens*.txt

# Convert bedgraph files to bigwig format
for FILE in $(find out/*.bedgraph -exec basename {} \;)
do
  ./bin/bedGraphToBigWig out/$FILE http://igbquickload.org/rnaseq/H_sapiens_Dec_2013/genome.txt converted/$(filename_without_extension $FILE).bw
done